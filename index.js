const express=require('express')
const bodyParser=require('body-parser')
const app=express()
const port=3030;
const {users}=require('./business_modules/router')
const mongoose=require('mongoose')

/// middleware prepare 
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))



app.use('/users',users)

const uri='mongodb://usersmanagement:usersmanagement123@ds239936.mlab.com:39936/uesr-managment'
mongoose.connect(uri, {useNewUrlParser: true}).then(res => {
    app.listen(port,()=>{
            console.log(`server is running on port ${port}`);
    })
})

