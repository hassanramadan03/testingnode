const express=require('express')
const router=express.Router()
const {createNewUser,getAllUser}=require('./controller')

router.post('/add',createNewUser)
router.get('/all',getAllUser)



module.exports=router